LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

GLOBAL_INCLUDES += $(LOCAL_DIR)

MODULE_SRCS += \
        $(LOCAL_DIR)/libavb/avb_chain_partition_descriptor.c \
        $(LOCAL_DIR)/libavb/avb_cmdline.c \
        $(LOCAL_DIR)/libavb/avb_crc32.c \
        $(LOCAL_DIR)/libavb/avb_crypto.c \
        $(LOCAL_DIR)/libavb/avb_descriptor.c \
        $(LOCAL_DIR)/libavb/avb_footer.c \
        $(LOCAL_DIR)/libavb/avb_hash_descriptor.c \
        $(LOCAL_DIR)/libavb/avb_hashtree_descriptor.c \
        $(LOCAL_DIR)/libavb/avb_kernel_cmdline_descriptor.c \
        $(LOCAL_DIR)/libavb/avb_property_descriptor.c \
        $(LOCAL_DIR)/libavb/avb_rsa.c \
        $(LOCAL_DIR)/libavb/avb_sha256.c \
        $(LOCAL_DIR)/libavb/avb_sha512.c \
        $(LOCAL_DIR)/libavb/avb_slot_verify.c \
        $(LOCAL_DIR)/libavb/avb_sysdeps_posix.c \
        $(LOCAL_DIR)/libavb/avb_util.c \
        $(LOCAL_DIR)/libavb/avb_vbmeta_image.c \
        $(LOCAL_DIR)/libavb/avb_version.c

include make/module.mk
